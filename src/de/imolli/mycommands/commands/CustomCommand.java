package de.imolli.mycommands.commands;

import org.bukkit.entity.Player;

public abstract class CustomCommand {

    private String label;

    public CustomCommand(String label) {
        this.label = label;
    }

    public abstract void execute(Player p, String args);

    public String getLabel() {
        return label;
    }
}
