package de.imolli.mycommands.commands;

import de.imolli.mycommands.MyCommands;
import de.imolli.mycommands.manager.CommandManager;
import de.imolli.mycommands.manager.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class COMMAND_removecommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(MessageManager.getMessage("MyCommands.console.notplayer"));
        }

        Player p = (Player) sender;

        if (!p.hasPermission("MyCommands.remove")) {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.noperm.msg"));
            return true;
        }

        if (args.length == 1) {

            String alias = args[0];

            if (CommandManager.getCommands().containsKey(alias)) {

                CommandManager.removeCommand(alias);
                CommandManager.deleteCommand(alias);

                p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.removecommand.removed").replaceAll("%alias%", alias));
                return true;
            } else {
                p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.removecommand.notexist").replaceAll("%alias%", alias));
                return true;
            }

        } else {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.cmd.error").replaceAll("%cmd%", "/removecommand [alias]"));
            return true;
        }
    }
}
