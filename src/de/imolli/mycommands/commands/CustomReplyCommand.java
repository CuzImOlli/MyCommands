package de.imolli.mycommands.commands;

import org.bukkit.entity.Player;

public class CustomReplyCommand extends CustomCommand {

    private String reply;

    public CustomReplyCommand(String label, String reply) {
        super(label);

        this.reply = reply;
    }

    @Override
    public void execute(Player p, String args) {

        p.sendMessage(reply);

    }

    public String getReply() {
        return reply;
    }
}
