package de.imolli.mycommands.commands;

import de.imolli.mycommands.MyCommands;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class COMMAND_mycommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {

        cs.sendMessage(" ");
        cs.sendMessage("    §cMyCommands §ev" + MyCommands.getInstance().getDescription().getVersion() + " §7by §e" + MyCommands.getInstance().getDescription().getAuthors().get(0));
        cs.sendMessage("    §7Description: §e" + MyCommands.getInstance().getDescription().getDescription());

        if (cs instanceof Player) {

            TextComponent component = new TextComponent("    §7More Info: ");
            TextComponent click = new TextComponent("§eClick here");
            click.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/mycommands.58078/"));
            click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("§eOpens the SpigotMC page of the plugin")));
            component.addExtra(click);

            ((Player) cs).spigot().sendMessage(component);

        }

        return true;
    }
}
