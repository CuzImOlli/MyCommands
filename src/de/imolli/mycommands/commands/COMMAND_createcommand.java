package de.imolli.mycommands.commands;

import de.imolli.mycommands.MyCommands;
import de.imolli.mycommands.manager.CommandManager;
import de.imolli.mycommands.manager.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class COMMAND_createcommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {

        if (!(cs instanceof Player)) {
            cs.sendMessage(MessageManager.getMessage("MyCommands.console.notplayer"));
            return true;
        }

        Player p = (Player) cs;

        if (!p.hasPermission("MyCommands.add")) {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.noperm.msg"));
            return true;
        }

        if (args.length >= 3) {
            if (args[0].equalsIgnoreCase("alias")) {

                String alias = args[1];
                String cmd = args[2];

                if (CommandManager.getCommands().containsKey(alias.toLowerCase())) {
                    p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.createcommand.alreadyexist").replaceAll("%alias%", alias));
                    return true;
                }

                CommandManager.addAliasCommand(alias, cmd);
                CommandManager.saveAliasCommand(alias, cmd);

                p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.createcommand.added").replaceAll("%alias%", alias).replaceAll("%cmd%", cmd));

                return true;
            } else if (args[0].equalsIgnoreCase("reply")) {

                p.sendMessage(MyCommands.getPrefix() + "§cComing soon!");
                return true;

            } else {
                p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.cmd.error").replaceAll("%cmd%", "/createcommand ['alias'|'reply'] [alias] [command|message]"));
                return true;
            }
        } else {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.cmd.error").replaceAll("%cmd%", "/createcommand ['alias'|'reply'] [alias] [command|message]"));
            return true;
        }
    }
}
