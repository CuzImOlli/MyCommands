package de.imolli.mycommands.commands;

import org.bukkit.entity.Player;

public class CustomAliasCommand extends CustomCommand {

    private String command;

    public CustomAliasCommand(String label, String command) {
        super(label);
        this.command = command;
    }

    @Override
    public void execute(Player p, String args) {
        p.performCommand(command + args);
    }

    public String getCommand() {
        return command;
    }
}
