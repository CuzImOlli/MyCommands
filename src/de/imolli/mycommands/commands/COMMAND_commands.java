package de.imolli.mycommands.commands;

import de.imolli.mycommands.MyCommands;
import de.imolli.mycommands.manager.CommandManager;
import de.imolli.mycommands.manager.MessageManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class COMMAND_commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {

        if (!(cs instanceof Player)) {
            cs.sendMessage(MessageManager.getMessage("MyCommands.console.notplayer"));
            return true;
        }

        Player p = (Player) cs;

        if (!p.hasPermission("MyCommands.list")) {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.noperm.msg"));
            return true;
        }

        if (CommandManager.getCommands().isEmpty()) {
            p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.commands.nocommands"));
            return true;
        }

        p.sendMessage(MyCommands.getPrefix() + MessageManager.getMessage("MyCommands.commands.header"));
        p.sendMessage(" ");

        for (String key : CommandManager.getCommands().keySet()) {

            CustomCommand cmd = CommandManager.getCommands().get(key);

            TextComponent base = new TextComponent("    §8» §a" + cmd.getLabel() + " §8[§7/" + ((CustomAliasCommand) cmd).getCommand() + "§8] ");

            TextComponent remove = new TextComponent("§c[Remove]");
            remove.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("§7Click to remove the command §e" + cmd.getLabel())));
            remove.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/removecommand " + cmd.getLabel()));

            base.addExtra(remove);

            p.spigot().sendMessage(base);

        }

        p.sendMessage(" ");

        return true;
    }
}
