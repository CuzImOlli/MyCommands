package de.imolli.mycommands;

import de.imolli.mycommands.commands.COMMAND_commands;
import de.imolli.mycommands.commands.COMMAND_createcommand;
import de.imolli.mycommands.commands.COMMAND_mycommands;
import de.imolli.mycommands.commands.COMMAND_removecommand;
import de.imolli.mycommands.manager.CommandManager;
import de.imolli.mycommands.manager.MessageManager;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class MyCommands extends JavaPlugin {

    private static String prefix;

    @Override
    public void onEnable() {
        loadConfig();
        registerManager();
        registerEventListener();
        registerCommands();

        //Include Metrics API from bStats
        Metrics metrics = new Metrics(this, 2762);
    }

    private static void registerEventListener() {
        Bukkit.getPluginManager().registerEvents(new CommandManager(), getInstance());
    }

    private static void registerManager() {
        MessageManager.init();
        MessageManager.loadConfig();
        CommandManager.init();
    }

    private static void registerCommands() {
        Bukkit.getPluginCommand("createcommand").setExecutor(new COMMAND_createcommand());
        Bukkit.getPluginCommand("removecommand").setExecutor(new COMMAND_removecommand());
        Bukkit.getPluginCommand("mycommands").setExecutor(new COMMAND_mycommands());
        Bukkit.getPluginCommand("commands").setExecutor(new COMMAND_commands());
    }

    private static void loadConfig() {
        MyCommands.getInstance().getConfig().options().copyDefaults(true);
        MyCommands.getInstance().getConfig().options().header("Configuration of MyCommands \n\n" +
                "Please note that editing the configurations while the server is running is not recommended.\n");

        MyCommands.getInstance().getConfig().addDefault("UpdateCheck", true);
        MyCommands.getInstance().getConfig().addDefault("UpdateNotify", true);

        MyCommands.getInstance().saveConfig();
    }

    public static String getPrefix() {
        return prefix;
    }

    public static void setPrefix(String prefix) {
        MyCommands.prefix = prefix;
    }

    public static Plugin getInstance() {
        return MyCommands.getPlugin(MyCommands.class);
    }

}