package de.imolli.mycommands.manager;

import de.imolli.mycommands.MyCommands;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

public class MessageManager {

    private static File file;
    private static YamlConfiguration config;
    private static HashMap<String, String> messages;

    public static void init() {
        messages = new HashMap<>();

        file = new File("plugins//MyCommands//messages.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                MyCommands.getInstance().getLogger().log(Level.SEVERE, "An error occurred while creating 'messages.yml'!");
                return;
            }
        }

        config = YamlConfiguration.loadConfiguration(file);

        config.options().copyDefaults(true);
        config.options().header("Messages configuration of MyCommands \n\n" +
                "Please note that editing the configurations while the server is running is not recommended.\n");

        config.addDefault("MyCommands.createcommand.alreadyexist", "§cThe alias §e'%alias%' §calready exist!");
        config.addDefault("MyCommands.createcommand.added", "§7Added the alias §e'%alias%' §7for the command §e'%cmd%'!");
        config.addDefault("MyCommands.removecommand.notexist", "§cSorry, but the alias '%alias%' does not exist!");
        config.addDefault("MyCommands.removecommand.removed", "§7You removed the alias §e%alias%!");
        config.addDefault("MyCommands.commands.nocommands", "§cSorry but no commands exist!");
        config.addDefault("MyCommands.commands.header", "§7Commands of §cMyCommands");

        config.addDefault("MyCommands.prefix", "&cMyCommands &8» ");
        config.addDefault("MyCommands.console.notplayer", "You must be a player to do that!");
        config.addDefault("MyCommands.reload", "&aReloaded all configurations.");
        config.addDefault("MyCommands.noperm.msg", "&cYou don't have enough permissions for that!");

        config.addDefault("MyCommands.error.msg", "&cAn error occurred!");
        config.addDefault("MyCommands.cmd.error", "&cUse %cmd%&c!");

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadConfig() {
        for (String key : config.getKeys(true)) {

            String value = config.getString(key);
            value = ChatColor.translateAlternateColorCodes('&', value);

            messages.put(key, value);
        }

        MyCommands.setPrefix(messages.getOrDefault("MyCommands.prefix", ""));
    }

    public static String getMessage(String name) {
        return messages.getOrDefault(name, name);
    }

}