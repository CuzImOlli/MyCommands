package de.imolli.mycommands.manager;

import de.imolli.mycommands.MyCommands;
import de.imolli.mycommands.commands.CustomAliasCommand;
import de.imolli.mycommands.commands.CustomCommand;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

public class CommandManager implements Listener {

    private static File file;
    private static YamlConfiguration config;
    private static HashMap<String, CustomCommand> commands;

    public static void init() {
        commands = new HashMap<>();

        file = new File("plugins//MyCommands//commands.yml");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            MyCommands.getInstance().getLogger().log(Level.SEVERE, "An error occurred while creating 'commands.yml'!");
        }


        config = YamlConfiguration.loadConfiguration(file);

        config.options().copyDefaults(true);
        config.options().header("Commands of MyCommands \n\n" +
                "Please note that editing the configurations while the server is running is not recommended.\n");

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
            MyCommands.getInstance().getLogger().log(Level.SEVERE, "An error occurred while saving 'commands.yml'!");
        }

        loadCommands();

    }

    private static void loadCommands() {

        for (String key : config.getKeys(false)) {

            String type = config.getString(key + ".type");

            if (type.equalsIgnoreCase("alias")) {

                String alias = config.getString(key + ".alias");
                String command = config.getString(key + ".command");

                addAliasCommand(alias, command);

            }
        }
    }

    @EventHandler
    public static void onCommand(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();

        String commandRaw = e.getMessage().split(" ")[0].replace("/", "");
        String[] argsRaw = e.getMessage().split(" ");
        String args = e.getMessage().replace(argsRaw[0], "");

        if (commands.containsKey(commandRaw)) {
            e.setCancelled(true);

            CustomCommand customCommand = commands.get(commandRaw);
            customCommand.execute(p, args);
        }
    }

    public static void addAliasCommand(String alias, String cmd) {

        commands.put(alias, new CustomAliasCommand(alias, cmd));

    }

    public static void removeCommand(String alias) {

        commands.remove(alias);

    }

    public static HashMap<String, CustomCommand> getCommands() {
        return commands;
    }

    public static void saveAliasCommand(String alias, String cmd) { // Saves the command to the config file
        config.set(alias.toLowerCase() + ".type", "alias");
        config.set(alias.toLowerCase() + ".alias", alias);
        config.set(alias.toLowerCase() + ".command", cmd);

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
            MyCommands.getInstance().getLogger().log(Level.SEVERE, "An error occurred while saving 'commands.yml'!");
        }
    }

    public static void deleteCommand(String alias) { //Delete command from config file
        config.set(alias.toLowerCase(), null);

        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
            MyCommands.getInstance().getLogger().log(Level.SEVERE, "An error occurred while saving 'commands.yml'!");
        }
    }
}